mod entities;
mod map;
pub(crate) mod state;
mod teams;
mod tuning;

use crate::teams::GameWorld;
use serde::Deserialize;
use std::collections::HashMap;
use std::rc::Rc;
use std::sync::mpsc;
use std::{cmp, str};
use twgame::console::Command;
use twgame::database::{DatabaseResult, DatabaseWrite};
use twgame::net_msg::ClNetMessage;
use twgame::{Game, Input};
use twmap::TwMap;
use twsnap::time::Instant;
use twsnap::uid::{PlayerUid, TeamId};
use twsnap::Snap;
use uuid::Uuid;
use vek::Vec2;

pub use crate::map::{coord, Map};
pub use crate::state::Bug;
#[derive(Deserialize, Debug)]
pub struct ThHeader {
    pub game_uuid: Uuid,
    pub prng_description: Option<String>,
    pub start_time: String,
    pub map_name: String,
    pub map_crc: String,
    pub map_sha256: Option<String>,
    pub version_minor: Option<String>,
    pub config: HashMap<String, String>,
    pub tuning: HashMap<String, String>,
}

impl ThHeader {
    pub fn from_buf(buf: &[u8]) -> Self {
        let buf = String::from_utf8_lossy(buf);
        serde_json::from_str(&buf).unwrap()
    }
}

#[derive(Debug)]
pub struct DdnetWorld {
    max_player_id: u32,
    players: HashMap<u32, PlayerUid>,
    teams: GameWorld,
}

impl DdnetWorld {
    /// Creates DDNet world from TwMap
    ///
    /// # Example usage
    ///
    /// ```rs
    /// // TODO
    /// ```
    pub fn new(
        map: &mut TwMap,
        sender: mpsc::Sender<DatabaseWrite>,
        receiver: mpsc::Receiver<DatabaseResult>,
    ) -> Result<DdnetWorld, String> {
        let map: Rc<Map> = Rc::new(map.try_into()?);
        let teams = teams::GameWorld::new(map, sender, receiver);
        Ok(DdnetWorld {
            max_player_id: 0,
            players: HashMap::new(),
            teams,
        })
    }

    /// Must be called before first tick. (TODO: ensure this with type system)
    /// Uses one `EntityList` for all teams instead of one `EntityList` per Team.
    pub fn enable_ddnet_tele_copat_mode(&mut self, enabled: bool) {
        self.teams.enable_ddnet_tele_copat_mode(enabled);
    }

    /// Must be called before first tick. (TODO: ensure this with type system)
    pub fn supply_prng_compat_data(&mut self, compat_data: &str) {
        self.teams.supply_prng_compat_data(compat_data);
    }

    pub fn enable_prng_compat_data_collection(&mut self) {
        self.teams.enable_prng_compat_data_collection();
    }

    /// Must be called after last tick. (TODO: ensure this with type system)
    pub fn retrieve_prng_compat_data(&mut self) -> Option<String> {
        self.teams.retrieve_prng_compat_data()
    }

    /// Returns used game bugs detected during run
    pub fn retrieve_bugs(&mut self) -> Vec<Bug> {
        self.teams.retrieve_bugs()
    }

    pub fn configure_with_teehistorian_parameters(&mut self, header: ThHeader) {
        self.teams.configure_with_teehistorian_parameters(header);
    }
}

impl Game for DdnetWorld {
    fn player_join(&mut self, id: u32) {
        let existing = self.players.insert(id, self.teams.player_join(id));
        self.max_player_id = cmp::max(self.max_player_id, id + 1);
        assert!(existing.is_none());
    }

    fn player_ready(&mut self, id: u32) {
        let player_uid = *self.players.get(&id).unwrap();
        self.teams.player_ready(player_uid);
    }

    fn player_input(&mut self, id: u32, input: Input) {
        let player_uid = *self.players.get(&id).unwrap();
        self.teams.player_input(player_uid, input);
    }

    fn player_leave(&mut self, id: u32) {
        let player_uid = self.players.remove(&id).unwrap();
        self.teams.player_leave(player_uid);
    }

    fn on_net_msg(&mut self, id: u32, msg: ClNetMessage) {
        let player_uid = *self.players.get(&id).unwrap();
        self.teams.on_net_msg(player_uid, msg);
    }

    fn on_command(&mut self, id: u32, command: Command) {
        let player_uid = *self.players.get(&id).unwrap();
        self.teams.on_command(player_uid, command);
    }

    fn swap_tees(&mut self, id1: u32, id2: u32) {
        let player_uid1 = *self.players.get(&id1).unwrap();
        let player_uid2 = *self.players.get(&id2).unwrap();
        self.teams.swap_tees(player_uid1, player_uid2);
    }

    fn cur_time(&self) -> Instant {
        self.teams.cur_time()
    }

    fn tick(&mut self) {
        self.teams.tick();
    }

    fn post_tick(&mut self) {
        self.teams.post_tick();
    }

    fn snap(&self, snapshot: &mut Snap) {
        self.teams.snap(snapshot);
    }

    fn tee_pos(&self, id: u32) -> Option<Vec2<i32>> {
        let Some(&player_uid) = self.players.get(&id) else {
            return None;
        };
        self.teams.tee_pos(player_uid)
    }

    fn max_tee_id(&self) -> u32 {
        self.max_player_id
    }

    fn set_tee_pos(&mut self, id: u32, pos: Option<Vec2<i32>>) {
        let player_uid = *self
            .players
            .get(&id)
            .expect("set_tee_pos called for non-existing player");

        self.teams.set_tee_pos(player_uid, pos);
    }

    fn player_team(&self, id: u32) -> u32 {
        let Some(&player_uid) = self.players.get(&id) else {
            return 0;
        };
        self.teams.player_team(player_uid).to_u32()
    }

    fn set_player_team(&mut self, id: u32, team: u32) {
        let player_uid = *self
            .players
            .get(&id)
            .expect("set_player_team called for non-existing player");
        self.teams
            .player_team_change(player_uid, TeamId::from_u32(team));
    }
    fn set_practice(&mut self, _team: u32, _on: bool) {}
}

use ddnet::ThHeader;
use std::collections::{HashMap, HashSet};
use std::sync::mpsc;
use teehistorian::chunks::{ConsoleCommand, PlayerId};
use teehistorian::{Chunk, Th};
use twgame::console::Command;
use twgame::database::{DatabaseResult, DatabaseWrite, Finishes, SaveTeam};
use twgame::net_msg::NetVersion;
use twgame::{Game, Input};
use twsnap::compat::ddnet::DemoWriter;
use twsnap::time::{Duration, Instant};
use twsnap::Snap;
use vek::Vec2;

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub enum ThPlayerEvent {
    /// optional event specifying protocol version for NetMessages
    JoinVer,
    Join,
    // PlayerInfo,
    // DDNetVersion,
    // finally joined the game
    InGame,
    Drop,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ThPlayer {
    event: ThPlayerEvent,
    version: NetVersion,
    position: Option<Vec2<i32>>,
    team: u32,
}

impl ThPlayer {
    fn new(version: NetVersion, event: ThPlayerEvent) -> Self {
        Self {
            event,
            version,
            position: None,
            team: 0,
        }
    }

    fn tee_pos(&self) -> Option<Vec2<i32>> {
        self.position
    }
}

struct ThTeam {
    practice: bool,
    // Some if save in progress
    save: Option<SaveTeam>,
    // true if load in progress
    load: bool,
}

impl ThTeam {
    fn new() -> Self {
        Self {
            practice: false,
            save: None,
            load: false,
        }
    }
}

trait ThCompat {
    fn next(&mut self) -> Result<Chunk, teehistorian::Error>;
    fn reset(&mut self);
}

// allow reset of last action
enum LastAction2 {
    Add(i32),
    Remove(i32),
    Chunk,
}

/// The PLAYER_READY was introduced in version 3
/// https://github.com/ddnet/ddnet/commit/3ea55dcc0ebc1c791e11cab0c268febe7e783504
struct ThMinor2Compat<'a> {
    reader: ThMinor4Compat<'a>,
    had_player_ready: HashSet<i32>,
    last_action: LastAction2,
}

impl<'a> ThMinor2Compat<'a> {
    fn new(reader: ThMinor4Compat<'a>) -> Self {
        Self {
            reader,
            had_player_ready: HashSet::new(),
            last_action: LastAction2::Chunk,
        }
    }
}

impl<'a> ThCompat for ThMinor2Compat<'a> {
    fn next(&mut self) -> Result<Chunk, teehistorian::Error> {
        let reader = &mut self.reader as *mut dyn ThCompat;
        let cid;
        match self.reader.next()? {
            Chunk::PlayerNew(p) if !self.had_player_ready.contains(&p.cid) => {
                // reset chunk (after block due to lifetime) and insert PlayerReady event
                self.had_player_ready.insert(p.cid);
                self.last_action = LastAction2::Add(p.cid);
                cid = p.cid;
            }
            next_chunk => {
                self.last_action = LastAction2::Chunk;
                if let Chunk::Drop(d) = &next_chunk {
                    self.had_player_ready.remove(&d.cid);
                    self.last_action = LastAction2::Remove(d.cid);
                } else {
                    self.last_action = LastAction2::Chunk;
                }
                return Ok(next_chunk);
            }
        }
        // # Safety
        //
        // This is safe, because we don't do anything with self anymore.
        // It would compile with polonius. Currently only possible to enable in nightly:
        //
        //     RUSTFLAGS="-Z polonius" cargo +nightly c
        //
        //  When polonius stabilizes this unsafe can be removed.
        unsafe {
            (*reader).reset();
        }
        Ok(Chunk::PlayerReady(PlayerId { cid }))
    }

    fn reset(&mut self) {
        match self.last_action {
            LastAction2::Add(cid) => self.had_player_ready.remove(&cid),
            LastAction2::Remove(cid) => self.had_player_ready.insert(cid),
            LastAction2::Chunk => false,
        };
        self.reader.reset();
    }
}

enum LastAction4 {
    Add(i32),
    NoNeedCompat,
    Chunk,
}

/// A wrapper around `teehistorian::ThBufRead` to fix backwards compat issues
///  - After a map vote, the `PlayerJoin` events are missing. Add them for Tees when the first one
///    was missing until the first PlayerJoin is observed
struct ThMinor4Compat<'a> {
    reader: &'a mut dyn ThCompat,
    need_compat: bool,
    had_player_join: HashSet<i32>,
    last_action: LastAction4,
}

impl<'a> ThMinor4Compat<'a> {
    fn new(reader: &'a mut (dyn ThCompat + 'a)) -> Self {
        Self {
            reader,
            need_compat: true,
            had_player_join: Default::default(),
            last_action: LastAction4::Chunk,
        }
    }
}

impl<'a> ThCompat for ThMinor4Compat<'a> {
    fn next(&mut self) -> Result<Chunk, teehistorian::Error> {
        let reader = self.reader as *mut dyn ThCompat;
        let cid;
        if !self.need_compat {
            self.last_action = LastAction4::Chunk;
            return self.reader.next();
        } else {
            let next_chunk = self.reader.next()?;
            if let Some(p_cid) = next_chunk.cid() {
                cid = p_cid;
                if cid < 0 || self.had_player_join.contains(&cid) {
                    self.last_action = LastAction4::Chunk;
                    return Ok(next_chunk);
                }
                if matches!(
                    next_chunk,
                    Chunk::Join(_) | Chunk::JoinVer6(_) | Chunk::JoinVer7(_)
                ) {
                    // likely no need for compat anymore
                    self.need_compat = false;
                    self.last_action = LastAction4::NoNeedCompat;
                    return Ok(next_chunk);
                }
                self.last_action = LastAction4::Add(cid);
                self.had_player_join.insert(cid);
            } else {
                self.last_action = LastAction4::Chunk;
                return Ok(next_chunk);
            }
        };
        // # Safety
        //
        // This is safe, because we don't do anything with self anymore.
        // It would compile with polonius. Currently only possible to enable in nightly:
        //
        //     RUSTFLAGS="-Z polonius" cargo +nightly c
        //
        //  When polonius stabilizes this unsafe can be removed.
        unsafe {
            (*reader).reset();
        }
        Ok(Chunk::Join(teehistorian::chunks::Join { cid }))
    }

    fn reset(&mut self) {
        match self.last_action {
            LastAction4::Add(cid) => {
                self.had_player_join.remove(&cid);
            }
            LastAction4::NoNeedCompat => self.need_compat = true,
            LastAction4::Chunk => {}
        }
        self.reader.reset();
    }
}

// implement the same interface, so that we can decide during runtime which to use
impl<R: teehistorian::ThBufRead> ThCompat for Th<R> {
    fn next(&mut self) -> Result<Chunk, teehistorian::Error> {
        self.next_chunk()
    }
    fn reset(&mut self) {
        self.reset_chunk();
    }
}

pub struct ThValidator<'a> {
    world: &'a mut dyn Game,
    players: Vec<Option<ThPlayer>>,
    teams: HashMap<u32, ThTeam>,
    // store last input per player not in player struct, since it survives rejoins
    last_input: Vec<Option<Input>>,
    peak_tee_count: u32,
    // store last player_id of messages PLAYER_DIFF, PLAYER_NEW, PLAYER_OLD to execute implicit ticks
    implicit_tick_player_id: Option<u32>,
    // the tick has to get validated after all Player{New,Diff,Old} are parsed and before the new
    // input from the next tick is passed to the Game world.
    last_tick_validated: bool,
    // after a TickSkip, a Load/Save result can return. This has to be passed to the game world,
    // before the tick is executed. Using this variable to dalay executing the tick until after the
    // database results
    delayed_tick: bool,
    snap: Snap,
    expected_finishes: Option<Vec<Finishes>>,
    got_finishes: Vec<Finishes>,
    database_request_receiver: mpsc::Receiver<DatabaseWrite>,
    database_result_sender: mpsc::Sender<DatabaseResult>,
}

impl<'a> ThValidator<'a> {
    pub fn new(
        world: &'a mut dyn Game,
        database_request_receiver: mpsc::Receiver<DatabaseWrite>,
        database_result_sender: mpsc::Sender<DatabaseResult>,
    ) -> Self {
        ThValidator {
            world,
            players: vec![],
            teams: HashMap::new(),
            last_input: vec![],
            peak_tee_count: 0,
            implicit_tick_player_id: None, // first tick is always implicit
            last_tick_validated: false,
            delayed_tick: false,
            snap: Snap::default(),
            expected_finishes: None,
            got_finishes: vec![],
            database_request_receiver,
            database_result_sender,
        }
    }

    /// optionally also verify that the correct writes to the database were made (finishes/saves/loads)
    pub fn with_expected_finishes(mut self, writes: Vec<Finishes>) -> Self {
        self.expected_finishes = Some(writes);
        self
    }

    fn format_console_command(cmd: &ConsoleCommand) -> Option<String> {
        let mut command = "/".to_owned();
        command.push_str(std::str::from_utf8(cmd.cmd).ok()?);
        for arg in cmd.args.iter() {
            command.push(' ');
            command.push_str(std::str::from_utf8(arg).ok()?);
        }
        Some(command)
    }

    #[must_use]
    fn check_tee_position_after_tick(
        &mut self,
        demo: &mut Option<&mut DemoWriter>,
        print: bool,
    ) -> bool {
        let mut correct = true;
        let world_max_player = self.world.max_tee_id();
        if world_max_player < self.peak_tee_count {
            let msg = format!("incorrect: Tees missing due to world max_player_id ({}) lower than in teehistorian checker ({})", world_max_player, self.peak_tee_count);
            if print {
                println!("{msg}");
            }
            if let Some(demo) = demo.as_mut() {
                demo.write_chat(&msg).unwrap();
            }
            correct = false;
        }
        for player_id in 0..world_max_player {
            let world_player_pos = self.world.tee_pos(player_id);
            if player_id < self.peak_tee_count {
                if let Some(player) = self.players[player_id as usize].as_ref() {
                    // don't check for player position for players in practice mode
                    let in_practice = self
                        .teams
                        .get(&player.team)
                        .map_or(false, |team| team.practice);
                    let expected_pos = player.tee_pos();
                    // check position for existing player
                    if world_player_pos != expected_pos {
                        self.world.set_tee_pos(player_id, expected_pos); // TODO: let the world decide if this was an unexpected diff?
                        if !in_practice {
                            correct = false;

                            let msg = format!("{}: player_id {}: incorrect tee position: found pos = {:?}, expected pos = {:?} ({:?}), diff: {:?}",
                                              self.world.cur_time(),
                                              player_id,
                                              world_player_pos,
                                              expected_pos,
                                              expected_pos.map(|p| p/32),
                                              expected_pos.zip(world_player_pos).map(|(a,b)| a - b)
                            );
                            if print {
                                println!("{msg}");
                            }
                            if let Some(demo) = demo.as_mut() {
                                demo.write_chat(&msg).unwrap();
                            }
                        }
                    }
                } else {
                    // no player exists for this player_id
                    if let Some(found_pos) = world_player_pos {
                        // player left in teehistorian file, but not in world, handle as fatal error for now
                        // might change this if it turns out to be not good
                        panic!("player_id {}: Found tee at pos {:?} even thought player slot is empty, incorrect world implementation 1", player_id, found_pos)
                    }
                }
            } else {
                // no player exists for this player_id TODO: deduplicate with above else?
                if let Some(found_pos) = world_player_pos {
                    // player left in teehistorian file, but not in world, handle as fatal error for now
                    // might change this if it turns out to be not good
                    panic!("player_id {}: Found tee at pos {:?} even thought player slot is empty, incorrect world implementation 2", player_id, found_pos)
                }
            }
        }
        // check for additional tees
        for player_id in world_max_player..self.peak_tee_count {
            if let Some(player) = self.players[player_id as usize].as_ref() {
                let expected_pos = player.tee_pos();
                // check position for existing player
                if expected_pos.is_some() {
                    let msg = format!(
                        "incorrect: {}: player_id {}: tee didn't join in world: new tee pos = {:?}",
                        self.world.cur_time(),
                        player_id,
                        expected_pos
                    );
                    if print {
                        println!("{msg}");
                    }
                    if let Some(demo) = demo.as_mut() {
                        demo.write_chat(&msg).unwrap();
                    }
                    self.world.set_tee_pos(player_id, expected_pos);
                    correct = false;
                }
            }
        }
        correct
    }

    #[must_use]
    fn check_player_team_after_tick(
        &mut self,
        demo: &mut Option<&mut DemoWriter>,
        print: bool,
    ) -> bool {
        let mut correct = true;
        for (player_id, p) in self.players.iter().enumerate() {
            if let Some(p) = p {
                let got_team = self.world.player_team(player_id as u32);
                if got_team != p.team {
                    correct = false;
                    let msg = format!(
                        "{}: player_id {}: incorrect team expected {}, got {}",
                        self.world.cur_time(),
                        player_id,
                        p.team,
                        got_team
                    );
                    if print {
                        println!("{msg}");
                    }
                    if let Some(demo) = demo.as_mut() {
                        demo.write_chat(&msg).unwrap();
                    }
                    self.world.set_player_team(player_id as u32, p.team)
                }
            }
        }
        correct
    }

    /// returns false if the validations fails
    fn maybe_validate_last_tick(
        &mut self,
        demo: &mut Option<&mut DemoWriter>,
        print: bool,
    ) -> bool {
        if !self.last_tick_validated {
            self.last_tick_validated = true;
            let tees_correct = self.check_tee_position_after_tick(demo, print);
            let teams_correct = self.check_player_team_after_tick(demo, print);
            // snap after setting tees to position from teehistorian file
            self.world.snap(&mut self.snap);
            if let Some(demo) = demo.as_mut() {
                demo.write_snapshot(self.world.cur_time().snap_tick(), &self.snap)
                    .unwrap();
            }
            tees_correct && teams_correct
        } else {
            true
        }
    }

    #[must_use]
    fn tick(&mut self, demo: &mut Option<&mut DemoWriter>, print: bool) -> bool {
        let correct = self.maybe_validate_last_tick(demo, print);

        self.world.tick();
        self.world.post_tick();
        self.snap.clear();

        self.last_tick_validated = false;
        correct
    }

    // returns whether PlayerNew, PlayerOld, PlayerDiff with given cid would result into a implicit
    // tick
    fn is_implicid_tick(&self, cid: u32) -> bool {
        if let Some(implicit_tick_player_id) = self.implicit_tick_player_id {
            if cid <= implicit_tick_player_id {
                return true;
            }
        }
        false
    }

    /// executes implicit tick(), if necessary
    #[must_use]
    fn implicit_tick(&mut self, cid: u32, demo: &mut Option<&mut DemoWriter>, print: bool) -> bool {
        let mut correct = true;
        if self.is_implicid_tick(cid) {
            correct = self.tick(demo, print);
        }
        self.implicit_tick_player_id = Some(cid);
        correct
    }

    /// returns true if every position was as expected
    pub fn validate<R: teehistorian::ThBufRead>(
        mut self,
        mut th: Th<R>,
        mut demo: Option<&mut DemoWriter>,
        print: bool,
    ) -> bool {
        let header = String::from_utf8_lossy(th.header().unwrap());
        let header: ThHeader = serde_json::from_str(&header).unwrap();

        if let Some(demo) = demo.as_mut() {
            demo.write_chat(
                "Demo created by Teehistorian replayer TwGame https://gitlab.com/ddnet-rs/TwGame",
            )
            .unwrap();
        }

        let mut th_compat: Box<dyn ThCompat> = match header.version_minor.as_deref() {
            None | Some("0" | "1" | "2") => {
                Box::new(ThMinor2Compat::new(ThMinor4Compat::new(&mut th)))
            }
            Some("3" | "4") => Box::new(ThMinor4Compat::new(&mut th)),
            _ => Box::new(th),
        };

        let mut correct = true;
        loop {
            match self.validate_next_chunk(&mut *th_compat, &mut demo, print) {
                Ok((true, c)) => {
                    correct = correct && c;
                    break;
                }
                Ok((false, c)) => correct = correct && c, // continue
                Err(_err) => {
                    //println!("{:?}", err);
                    break;
                }
            }
        }
        if print {
            println!("Got database writes: {:?}", self.got_finishes);
        }
        if let Some(writes) = self.expected_finishes {
            if writes != self.got_finishes {
                correct = false;
                if print {
                    println!(
                        "incorrect: Unexpected database writes: expected {:?}, got {:?}",
                        writes, self.got_finishes
                    )
                }
            }
        }
        correct
    }

    // returns whether everything was correct. False, if anything unexpected occurred
    fn process_database_queries(&mut self, print: bool) -> bool {
        let mut correct = true;
        for database_query in self.database_request_receiver.try_iter() {
            if print {
                println!("{:?}", database_query);
            }
            match database_query {
                DatabaseWrite::Save(team_id, save_team) => {
                    let team = self.teams.get_mut(&team_id).unwrap();
                    if team.load {
                        correct = false;
                        if print {
                            println!("incorrect: GameWorld requested Save for team, while Load was in progress")
                        }
                    }
                    if team.save.is_some() {
                        correct = false;
                        if print {
                            println!("incorrect: GameWorld requested Save for team, while Save was in progress")
                        }
                    }
                    team.save = Some(save_team);
                }
                DatabaseWrite::FinishTee(f) => self.got_finishes.push(Finishes::FinishTee(f)),
                DatabaseWrite::FinishTeam(f) => self.got_finishes.push(Finishes::FinishTeam(f)),
                DatabaseWrite::Load(team_id) => {
                    let team = self.teams.get_mut(&team_id).unwrap();
                    if team.load {
                        correct = false;
                        if print {
                            println!("incorrect: GameWorld requested Load for team, while Load was in progress")
                        }
                    }
                    if team.save.is_some() {
                        correct = false;
                        if print {
                            println!("incorrect: GameWorld requested Load for team, while Save was in progress")
                        }
                    }
                    team.load = true;
                }
            }
        }
        correct
    }

    fn join_ver(&mut self, demo: &mut Option<&mut DemoWriter>, cid: i32, net_version: NetVersion) {
        assert!(0 <= cid);
        let cid = cid as usize;
        if cid >= self.players.len() {
            self.players.resize(cid + 1, None);
            self.last_input.resize(cid + 1, None);
        } else if self.players[cid]
            .as_ref()
            .map(|p| p.event != ThPlayerEvent::Drop)
            .unwrap_or(false)
        {
            let msg = format!("player {cid} (0.6) reconnected before timeout");
            if let Some(demo) = demo.as_mut() {
                demo.write_chat(&msg).unwrap();
            }
            println!("{msg}");
            return;
        }

        self.players[cid] = Some(ThPlayer::new(net_version, ThPlayerEvent::JoinVer));
    }

    fn join(&mut self, cid: i32) {
        // player joined on engine level (via 0.6 if not previously specified via JoinVer6/JoinVer7)
        assert!(0 <= cid);
        let cid = cid as usize;
        if cid >= self.players.len() {
            self.players.resize(cid + 1, None);
            self.last_input.resize(cid + 1, None);
        }
        if let Some(player) = self.players[cid].as_mut() {
            // Join must be preceded by JoinVer
            assert_eq!(player.event, ThPlayerEvent::JoinVer);
            player.event = ThPlayerEvent::Join;
        } else {
            self.players[cid] = Some(ThPlayer::new(NetVersion::Unknown, ThPlayerEvent::Join));
        }
        self.world.player_join(cid as u32);
    }

    fn validate_next_chunk(
        &mut self,
        th: &mut dyn ThCompat,
        demo: &mut Option<&mut DemoWriter>,
        print: bool,
    ) -> Result<(bool, bool), ()> {
        let chunk = match th.next() {
            Ok(chunk) => chunk,
            Err(teehistorian::Error::Eof) => {
                // Eos chunk missing, don't panic. It's fine.
                return Ok((true, true));
            }
            Err(_) => {
                // some other error occured, don't yet panic. But say that reproduction is incorrect
                return Ok((true, false));
            }
        };
        let mut correct = true;

        if self.delayed_tick
            && !matches!(
                chunk,
                Chunk::TeamLoadSuccess(_)
                    | Chunk::TeamLoadFailure(_)
                    | Chunk::TeamSaveSuccess(_)
                    | Chunk::TeamSaveFailure(_)
            )
        {
            self.delayed_tick = false;
            correct = self.tick(demo, print) && correct;
        }

        if !matches!(
            chunk,
            Chunk::PlayerNew(_)
                | Chunk::PlayerOld(_)
                | Chunk::PlayerDiff(_)
                | Chunk::TeamLoadFailure(_)
                | Chunk::TeamLoadSuccess(_)
                | Chunk::TeamSaveFailure(_)
                | Chunk::TeamSaveSuccess(_)
                | Chunk::TeamPractice(_)
                | Chunk::PlayerTeam(_)
                | Chunk::UnknownEx(_)
                | Chunk::Eos
        ) {
            correct = self.maybe_validate_last_tick(demo, print) && correct;
        }
        if print {
            println!("{}: {:?}", self.world.cur_time(), chunk);
        }

        match chunk {
            Chunk::Eos => {
                return Ok((true, correct));
            }
            // getting new input
            // next player to join joins via 0.6 protocol
            Chunk::JoinVer6(p) => self.join_ver(demo, p.cid, NetVersion::V06),
            // next player to join joins via 0.7 vanilla protocol
            Chunk::JoinVer7(p) => self.join_ver(demo, p.cid, NetVersion::V07),
            // ignore rejoins
            Chunk::RejoinVer6(_) => {}
            Chunk::Join(p) => {
                self.peak_tee_count = self.peak_tee_count.max(p.cid as u32 + 1);
                self.join(p.cid)
            }
            Chunk::Drop(p) => {
                // player left on engine level
                assert!(0 <= p.cid && p.cid < self.players.len() as i32);
                if let Some(player) = self.players[p.cid as usize].as_mut() {
                    // check if only joined players can drop
                    assert_ne!(player.event, ThPlayerEvent::Drop);
                    player.event = ThPlayerEvent::Drop;
                    self.world.player_leave(p.cid as u32);
                } else {
                    //TODO: figure out why I can't panic!("Non-existing player dropped from engine");
                }
            }

            Chunk::InputNew(inp) => {
                assert!(0 <= inp.cid && inp.cid < self.players.len() as i32);
                //assert_eq!(self.last_input[inp.cid as usize], None);
                let input = Input::from(inp.input);
                self.world.player_input(inp.cid as u32, input.clone());
                self.last_input[inp.cid as usize] = Some(input);
            }
            Chunk::InputDiff(inp) => {
                assert!(0 <= inp.cid && inp.cid < self.players.len() as i32);
                assert_ne!(self.last_input[inp.cid as usize], None);
                if let Some(cur_inp) = self.last_input[inp.cid as usize].as_mut() {
                    cur_inp.add_input_diff(inp.dinput);
                    self.world.player_input(inp.cid as u32, cur_inp.clone());
                }
            }

            Chunk::NetMessage(msg) => {
                assert!(0 <= msg.cid && msg.cid < self.players.len() as i32);
                if let Some(p) = self.players[msg.cid as usize].as_mut() {
                    match twgame::net_msg::parse_net_msg(msg.msg, &mut p.version) {
                        Ok(net_msg) => self.world.on_net_msg(msg.cid as u32, net_msg),
                        Err(err) => {
                            if print {
                                println!(
                                    "Error on NetMessage ({}): {}",
                                    err,
                                    String::from_utf8_lossy(msg.msg),
                                );
                            }
                        }
                    }
                } else {
                    panic!(
                        "player_id {}: NetMessage event for non-existing player",
                        msg.cid
                    );
                }
            }
            Chunk::ConsoleCommand(cmd) => {
                assert!(-1 <= cmd.cid && cmd.cid < self.players.len() as i32);
                if cmd.cid == -1 {
                    // initiated by server, ignore for now.
                } else {
                    let cid = cmd.cid as u32;
                    if let Some(demo) = demo.as_mut() {
                        if let Some(command) = Self::format_console_command(&cmd) {
                            // TODO: store PlayerUids in validator and pass correct one here:
                            demo.write_player_chat(cmd.cid, &command).unwrap();
                        }
                    }
                    match Command::from_teehistorian(cmd) {
                        None => {
                            if print {
                                println!("Ignoring console command")
                            }
                        }
                        Some(cmd) => {
                            // ignore team command in the first three seconds. Hacky workaround to
                            // tests running for a behavior that might change in future ddnet versions
                            if matches!(cmd, Command::Team(_))
                                && !self
                                    .world
                                    .cur_time()
                                    .duration_passed_since(Instant::zero(), Duration::from_secs(3))
                            {
                                // ignore command for now
                            } else {
                                self.world.on_command(cid, cmd);
                            }
                        }
                    }
                    // save/load could be initiated. update internal state
                    correct = self.process_database_queries(print) && correct;
                }
            }

            // checking input
            Chunk::PlayerReady(p) => {
                assert!(0 <= p.cid && p.cid < self.players.len() as i32);
                if let Some(player) = self.players[p.cid as usize].as_mut() {
                    assert_eq!(player.event, ThPlayerEvent::Join);
                    player.event = ThPlayerEvent::InGame;
                    self.world.player_ready(p.cid as u32);
                } else {
                    panic!(
                        "player_id {}: PlayerReady event for non-existing player",
                        p.cid
                    );
                }
            }
            Chunk::PlayerNew(p) => {
                assert!(0 <= p.cid && p.cid < self.players.len() as i32);
                correct = self.implicit_tick(p.cid as u32, demo, print) && correct;

                if let Some(player) = self.players[p.cid as usize].as_mut() {
                    // ThPlayerEvent::Join only possible as long as teehistorian doesn't record player_ready messages+
                    assert!(matches!(player.event, ThPlayerEvent::InGame));
                    player.event = ThPlayerEvent::InGame;

                    assert_eq!(player.position, None);
                    player.position = Some(Vec2::new(p.x, p.y));
                } else {
                    panic!(
                        "player_id {}: PlayerNew event for non-existing player",
                        p.cid
                    );
                }
                assert_ne!(self.players[p.cid as usize], None);
            }
            Chunk::PlayerDiff(p) => {
                assert!(0 <= p.cid && p.cid < self.players.len() as i32);
                correct = self.implicit_tick(p.cid as u32, demo, print) && correct;
                if let Some(player) = self.players[p.cid as usize].as_mut() {
                    assert_eq!(player.event, ThPlayerEvent::InGame);
                    if let Some(pos) = player.position.as_mut() {
                        *pos += Vec2::new(p.dx, p.dy);
                    } else {
                        panic!(
                            "player_id {}: PlayerDiff event before PlayerNew event",
                            p.cid
                        );
                    }
                } else {
                    panic!(
                        "player_id {}: PlayerDiff event for non-existing player",
                        p.cid
                    );
                }
            }
            Chunk::PlayerOld(p) => {
                assert!(0 <= p.cid && p.cid < self.players.len() as i32);
                correct = self.implicit_tick(p.cid as u32, demo, print) && correct;

                if let Some(player) = self.players[p.cid as usize].as_mut() {
                    // TODO: figure out why I can't do the following two checks
                    /*
                    if player.event != ThPlayerEvent::InGame && player.event != ThPlayerEvent::Drop
                    {
                        panic!("player_id {}: PlayerOld event in wrong state found {:?}, expected InGame or Drop", p.cid, player.event)
                    }
                    assert_ne!(player.tee_pos(), None); // Player must exist
                    */
                    player.position = None;
                    if player.event == ThPlayerEvent::Drop {
                        self.players[p.cid as usize] = None;
                    }
                } else {
                    panic!(
                        "player_id {}: PlayerOld event for non-existing player",
                        p.cid
                    )
                }
            }

            Chunk::PlayerTeam(p) => {
                assert!(0 <= p.cid && p.cid <= self.players.len() as i32);
                let team = p.team.try_into().unwrap();
                if let Some(player) = self.players[p.cid as usize].as_mut() {
                    player.team = team;
                }
                self.teams.entry(team).or_insert_with(ThTeam::new);
            }

            Chunk::TickSkip(t) => {
                // explicits n+1 ticks
                assert!(t.dt >= 0);
                for _ in 0..(t.dt) {
                    correct = self.tick(demo, print) && correct;
                }
                self.implicit_tick_player_id = None;
                self.delayed_tick = true;
            }
            Chunk::PlayerSwap(ps) => {
                let id1: u32 = ps.cid1.try_into().unwrap();
                let id2: u32 = ps.cid2.try_into().unwrap();
                self.world.swap_tees(id1, id2);
            }
            Chunk::TeamLoadFailure(team) => {
                let team_id = team.team.try_into().unwrap();
                if let Some(team) = self.teams.get_mut(&team_id) {
                    if !team.load {
                        if print {
                            println!("incorrect: TeamLoadFailure in Teehistorian, but GameWorld didn't request save");
                        }
                        correct = false;
                    }
                    team.load = false;
                    if let Err(_) = self
                        .database_result_sender
                        .send(DatabaseResult::LoadFailure(team_id))
                    {
                        correct = false;
                        if print {
                            println!("incorrect: TeamLoadFailure: GameWorld closed DatabaseReader");
                        }
                    }
                } else {
                    correct = false;
                    if print {
                        println!("incorrect: TeamLoadFailure: load for non-existing team");
                    }
                }
            }
            Chunk::TeamLoadSuccess(team_save) => {
                let team_id = team_save.team.try_into().unwrap();
                if let Some(team) = self.teams.get_mut(&team_id) {
                    if !team.load {
                        correct = false;
                        if print {
                            println!("incorrect: TeamLoadSuccess in Teehistorian, but GameWorld didn't request load");
                        }
                    }
                    team.load = false;
                    if let Err(_) = self
                        .database_result_sender
                        .send(DatabaseResult::LoadSuccess(
                            team_id,
                            SaveTeam::from_buf(team_save.save),
                        ))
                    {
                        correct = false;
                        if print {
                            println!("incorrect: TeamLoadSuccess: GameWorld closed DatabaseReader");
                        }
                    }
                } else {
                    correct = false;
                    if print {
                        println!("incorrect: TeamLoadSuccess: load for non-existing team");
                    }
                }
            }
            Chunk::TeamSaveFailure(team) => {
                // pass save failure to team, verify that save was requested
                let team_id = team.team.try_into().unwrap();
                if let Some(team) = self.teams.get_mut(&team_id) {
                    if team.save.take().is_none() {
                        correct = false;
                        if print {
                            println!("incorrect: TeamSaveFailure in Teehistorian, but GameWorld didn't request save");
                        }
                    }
                } else {
                    correct = false;
                    if print {
                        println!("incorrect: TeamSaveFailure: save for non-existing team");
                    }
                }

                if let Err(_) = self
                    .database_result_sender
                    .send(DatabaseResult::SaveFailure(team_id))
                {
                    correct = false;
                    if print {
                        println!("incorrect: TeamSaveFailure: GameWorld closed DatabaseReader");
                    }
                }
            }
            Chunk::TeamSaveSuccess(team_save) => {
                // pass save success to team, verify that save was requested with that exact save string
                let team_id = team_save.team.try_into().unwrap();
                let teehistorian_save = SaveTeam::from_buf(team_save.save);
                if let Some(team) = self.teams.get_mut(&team_id) {
                    if let Some(requested_save) = team.save.take() {
                        if !requested_save.compat_eq(&teehistorian_save) {
                            if print {
                                println!(
                                    "incorrect: Save game_state differs from teehistorian validator {:?}, teehistorian {:?}",
                                    requested_save,
                                    teehistorian_save
                                );
                                requested_save.print_diff(&teehistorian_save);
                            }
                            correct = false;
                        }
                    } else {
                        correct = false;
                        if print {
                            println!("incorrect: TeamSaveSuccess in Teehistorian, but GameWorld didn't request save");
                        }
                    }
                } else {
                    correct = false;
                    if print {
                        println!("incorrect: TeamSaveSuccess: save for non-existing team");
                    }
                }
                if let Err(_) = self
                    .database_result_sender
                    .send(DatabaseResult::SaveSuccess(team_id, teehistorian_save))
                {
                    correct = false;
                    if print {
                        println!("incorrect: TeamSaveSuccess: GameWorld closed DatabaseReader");
                    }
                }
            }
            Chunk::TeamPractice(team_practice) => {
                let team_id = team_practice.team as u32;
                let practice = team_practice.practice != 0;
                self.teams.get_mut(&team_id).unwrap().practice = practice;
                self.world.set_practice(team_id, practice)
            }
            // TBD
            _ => {}
        }
        Ok((false, correct))
    }
}

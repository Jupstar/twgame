// check if the simplified version of count input is correct
/* Original function src/game/gamecore.h
 * https://github.com/ddnet/ddnet/blob/e5e2e46da295cec9587c28a0b8a57a9494ff68fb/src/game/gamecore.h#L283-L300
inline CInputCount CountInput(int Prev, int Cur)
{
    CInputCount c = {0, 0};
    Prev &= INPUT_STATE_MASK;
    Cur &= INPUT_STATE_MASK;
    int i = Prev;

    while(i != Cur)
    {
        i = (i + 1) & INPUT_STATE_MASK;
        if(i & 1)
            c.m_Presses++;
        else
            c.m_Releases++;
    }

    return c;
}
*/

// only the first integer is relevant
fn count_input_orig(mut prev: i32, mut cur: i32) -> i32 {
    let mut c = 0;
    prev &= 63;
    cur &= 63;
    let mut i = prev;

    while i != cur {
        i = (i + 1) & 63;
        if i & 1 != 0 {
            c += 1
        }
    }

    c
}

fn count_input_simple(prev: i32, cur: i32) -> i32 {
    ((((cur & 63) - (prev & 63)) & 63) + (cur & 1)) / 2
}

#[test]
fn equal() {
    for i in 0..64 {
        for j in 0..64 {
            assert_eq!(count_input_orig(i, j), count_input_simple(i, j));
        }
    }
}

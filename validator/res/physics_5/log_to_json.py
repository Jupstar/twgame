#!/usr/bin/python3

import sys
import re
import sqlite3
import json

# 2023-04-16 00:58:19 I sql: SQLite statement: INSERT OR IGNORE INTO record_race(	Map, Name, Timestamp, Time, Server, 	cp1, cp2, cp3, cp4, cp5, cp6, cp7, cp8, cp9, cp10, cp11, cp12, cp13, 	cp14, cp15, cp16, cp17, cp18, cp19, cp20, cp21, cp22, cp23, cp24, cp25, 	GameID, DDNet7) VALUES ('physics_test_5', 'Zwelf', DATETIME('2023-04-16 00:58:19', 'utc'), 269.84, 'DATA', 	0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 	0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 	'a52f1009-2b68-4419-8dba-6101390f6390', 0)

def convert(filename):
    conn = sqlite3.connect(":memory:")
    c = conn.cursor()
    # TODO: teamrank
    c.execute("CREATE TABLE IF NOT EXISTS record_race ("
			"Map VARCHAR(128) COLLATE BINARY NOT NULL, "
			"Name VARCHAR(16) COLLATE BINARY NOT NULL, "
			"Timestamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, "
			"Time FLOAT DEFAULT 0, "
			"Server CHAR(4), " +
			"".join(f"cp{i + 1} FLOAT DEFAULT 0, " for i in range(25)) +
			"GameID VARCHAR(64), "
			"DDNet7 BOOL DEFAULT FALSE"
		");")
    for line in open(filename):
        if 'INSERT OR IGNORE INTO record_race' in line:
            _, stmt = line.split('sql: SQLite statement: ')
            c.execute(stmt)
    conn.commit()
    c.execute("SELECT Name, Time FROM record_race ORDER BY Timestamp")
    out = c.fetchall()
    out = [{"players": [name], "time": time} for name, time in out]
    out = json.dumps({"finishes": out}, indent=4)
    out_filename = filename.rsplit('.', 1)[0] + ".json"
    with open(out_filename, "w") as f:
        f.write(out)

def main():
    for filename in sys.argv[1:]:
        print(filename)
        convert(filename)

if __name__ == '__main__':
    main()

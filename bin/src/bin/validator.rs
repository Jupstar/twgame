use clap::Parser;
use ddnet::ThHeader;
use std::fs::File;
use std::sync::mpsc;
use teehistorian::{Th, ThBufReader};
use twvalidator::ThValidator;

/// Teehistorian validator
///
/// Gets all player inputs from teehistorian file and simulate it on the given map. Uses player
/// positions and some other validation information from teehistorian file to check if physics
/// implementation is correct.
#[derive(Debug, Parser)]
#[command(author, version, about)]
struct Opt {
    /// path to map file to do physics simulation on
    #[arg(name = "map")]
    twmap_file: String,

    /// path to teehistorian file
    teehistorian_file: String,
}

fn main() {
    let opt = Opt::parse();
    let f = File::open(opt.teehistorian_file).unwrap();

    let mut th = Th::parse(ThBufReader::new(f)).unwrap();
    println!("{}", String::from_utf8_lossy(th.header().unwrap()));
    let mut map = twmap::TwMap::parse_file(opt.twmap_file).unwrap();

    let (request_sender, request_receiver) = mpsc::channel();
    let (response_sender, response_receiver) = mpsc::channel();
    let mut world = ddnet::DdnetWorld::new(&mut map, request_sender, response_receiver).unwrap();

    let header = ThHeader::from_buf(th.header().unwrap());
    world.configure_with_teehistorian_parameters(header);

    let validator = ThValidator::new(&mut world, request_receiver, response_sender);
    println!("Start validation");
    validator.validate(th, None, true);
}

use std::fs::File;
use std::io::BufWriter;
use std::sync::mpsc;
//use demo::DemoReader;
use clap::Parser;
use ddnet::ThHeader;
use sha2::{Digest, Sha256};
use teehistorian::{Th, ThBufReader};
use twsnap::compat::ddnet::{DemoKind, DemoMapHash, DemoWriter};
use twvalidator::ThValidator;

/// Teehistorian to demo converter
#[derive(Debug, Parser)]
#[command(author, version, about)]
struct Opt {
    /// path to input teehistorian file
    from: String,
    /// path to map file for physics simulation
    map: String,
    /// path to demo output file
    to: String,
}

fn main() {
    let opt = Opt::parse();
    let f = File::open(opt.from).unwrap();

    let mut th = Th::parse(ThBufReader::new(f)).unwrap();

    let bytes = std::fs::read(&opt.map).unwrap(); // Vec<u8>
    let mut hasher = Sha256::new();
    hasher.update(bytes);
    let map_sha256 = hasher.finalize();
    let mut map = twmap::TwMap::parse_file(opt.map).unwrap();

    let (request_sender, request_receiver) = mpsc::channel();
    let (response_sender, response_receiver) = mpsc::channel();
    let mut world = ddnet::DdnetWorld::new(&mut map, request_sender, response_receiver).unwrap();

    let header = ThHeader::from_buf(th.header().unwrap());

    let to = BufWriter::new(File::create(opt.to).unwrap());
    let mut demo = DemoWriter::new(
        to,
        DemoKind::Server,
        &header.start_time,
        "0.6 TwGame", // TODO
        &header.map_name,
        None,
        DemoMapHash::Sha256(map_sha256.into()),
        0,
    )
    .unwrap();

    world.configure_with_teehistorian_parameters(header);

    let validator = ThValidator::new(&mut world, request_receiver, response_sender);
    let success = validator.validate(th, Some(&mut demo), false);
    println!("Success: {success}");
}

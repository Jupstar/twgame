use clap::Parser;
use rusqlite::types::Value;
use rusqlite::{params, Connection};
use serde::Deserialize;
use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::BufWriter;
use std::path::{Path, PathBuf};
use std::str::FromStr;
use std::sync::mpsc;
use teehistorian::{Th, ThBufReader};
use twvalidator::ThValidator;
use uuid::Uuid;
use xdg::BaseDirectories;

/// This replayer is for both going through all new teehistorian files and add them to a database
/// including map information on how to replay them.
///  * If it has gone through the demo it also stores saves/loads happening in the replay
///  * It also stores all finishes observed in the replay
///  * It also outputs whether known cheats were observed (skipping kill tiles/not touching defined
///    map areas)
///  * It can also be instructed which runs within a map can still be replayed if replayed with a
///    different map
///
/// The map replayer is capable to download maps from heinrich5991's map archive, Zwelf's archive
/// for replaying maps (all maps from ddnet-maps including older versions)
///
/// The map replayer is capable of finding the sha256 of a map if it is available in one archive
/// mentioned above.
///
/// It can be instructed to go through specific teehistorian files and try to replay them again
/// Depending on
///  * On a map by name
///  * On a map by sha256
///  * In a specified time range
///  * That wasn't replayable until now.
///  * That match a specific ddnet version
///
/// This replayer only correctly on teehistorian files that are name {game_uuid}.teehistorian
#[derive(Debug, Parser)]
#[command(author, version, about)]
struct Opt {
    /// Select Teehistorian files with this map name to to do physics simulation on
    // TODO: what about map renames?
    #[arg(short, long)]
    map: Option<String>,

    /// Select Teehistorian files with this map sha256 to to do physics simulation on
    #[arg(long)]
    sha: Option<String>,

    /// replay on all teehistorian files
    #[arg(long)]
    all: bool,

    /// Do physics simulation only on teehistorian files recorded after this time
    #[arg(short, long)]
    after: Option<String>,

    /// Do physics simulation only on teehistorian files recorded before this time
    #[arg(short, long)]
    before: Option<String>,

    #[arg(short, long)]
    /// replay this specific game by uuid
    uuid: Option<Uuid>,

    /// Do physics simulation only on teehistorian files recorded with this ddnet version
    #[arg(long)]
    ddnet: Option<String>,

    /// path to recursively traverse for teehistorian files to index
    #[arg(short, long)]
    index: Option<String>,

    /// Path to map to override map
    #[arg(short, long)]
    override_map: Option<String>,

    #[arg(short, long)]
    /// Path to demo output
    // TODO: configure this: only output demo runs
    demo: Option<String>,
}

/// sqlite schema is optimized more on size the path name doesn't contain the `uuid.teehistorian` if it
/// ends with `/`. This way it can deduplicate a lot of the paths. uuid and map_sha256 are stored in
/// binary. timestamp is converted to an integer.
///
/// path
///  * id | name
/// teehistorian
///  * uuid | path_id | map_name | map_sha256 | broken | verified | game_info
/// game_info can contain things like save/load/finishes/players

/// .cache/twgame-replayer/maps/sha256.map
/// .local/share/twgame-replayer/index.sqlite

fn setup_db(path: PathBuf) -> rusqlite::Result<Connection> {
    let conn = Connection::open(path)?;
    conn.execute(
        "CREATE TABLE IF NOT EXISTS path (\
            id INTEGER PRIMARY KEY,\
            name TEXT NOT NULL\
        ) STRICT;",
        [],
    )?;
    conn.execute(
        "CREATE TABLE IF NOT EXISTS map (\
            id INTEGER PRIMARY KEY,\
            name TEXT NOT NULL,\
            sha256 BLOB,\
            crc INTEGER NOT NULL,\
        ) STRICT;",
        [],
    )?;
    conn.execute(
        "CREATE TABLE IF NOT EXISTS teehistorian (\
            uuid BLOB NOT NULL,\
            path_id INTEGER NOT NULL,\
            date INTEGER NOT NULL,\
            map INTEGER NOT NULL,\
            game_info TEXT\
        ) STRICT;",
        [],
    )?;
    Ok(conn)
}

#[derive(Deserialize, Debug)]
struct ThHeader {
    //pub game_uuid: Uuid,
    pub start_time: String,
    pub map_name: String,
    pub map_sha256: Option<String>,
    pub map_crc: String,
}
fn get_all_existing_uuids(conn: &Connection) -> rusqlite::Result<HashSet<Uuid>> {
    let mut stmt = conn.prepare("SELECT uuid FROM teehistorian;")?;
    let uuids: Result<_, _> = stmt
        .query_map([], |row| Ok(Uuid::from_bytes(row.get(0)?)))?
        .collect();
    uuids
}
fn get_all_known_paths(conn: &Connection) -> rusqlite::Result<HashMap<String, u32>> {
    let mut stmt = conn.prepare("SELECT id, name FROM path;")?;
    let paths: Result<_, _> = stmt
        .query_map([], |row| Ok((row.get(1)?, row.get(0)?)))?
        .collect();
    paths
}

#[allow(clippy::type_complexity)]
fn _get_all_known_maps(
    conn: &Connection,
) -> rusqlite::Result<HashMap<u32, (String, Option<[u8; 32]>)>> {
    let mut stmt = conn.prepare("SELECT id, name, sha256 FROM map;")?;
    let maps: Result<_, _> = stmt
        .query_map([], |row| Ok((row.get(0)?, (row.get(1)?, row.get(2)?))))?
        .collect();
    maps
}
fn teehistorian_file_uuid(entry: &Path) -> Option<Uuid> {
    if entry.extension()? != "teehistorian" {
        return None;
    }
    let Some(name) = entry.file_stem() else {
        return None;
    };
    Uuid::from_str(name.to_str()?).ok()
}
fn teehistorian_parse_header(filename: &Path) -> Result<ThHeader, String> {
    let f = File::open(filename).map_err(|err| err.to_string())?;
    match Th::parse(ThBufReader::new(f)) {
        Ok(mut th_reader) => {
            // it is a teehistorian file, now try to extract the header
            match th_reader.header() {
                Ok(header) => {
                    // now try to parse the header
                    match serde_json::from_slice::<ThHeader>(header) {
                        Ok(th_header) => Ok(th_header),
                        Err(err) => Err(format!(
                            "{:?} Error parsing header with serde: {}",
                            filename, err
                        )),
                    }
                }
                Err(teehistorian::Error::IoError(err)) => Err(err.to_string()),
                Err(err) => {
                    // non-parseable header
                    Err(format!("{:?} Error parsing header: {}", filename, err))
                }
            }
        }
        Err(teehistorian::Error::IoError(err)) => Err(err.to_string()),
        Err(err) => {
            // non-parseable header
            Err(format!("{:?} Error parsing header: {}", filename, err))
        }
    }
}
fn traverse_teehistorian_files(conn: &Connection, path: &str) -> rusqlite::Result<()> {
    let mut path = PathBuf::from_str(path).unwrap();
    path = path.canonicalize().expect("Can't get canonical path");
    let mut paths = get_all_known_paths(conn).unwrap();
    let uuids = get_all_existing_uuids(conn).unwrap();
    let new_teehistorians = walkdir::WalkDir::new(path)
        .into_iter()
        // filter out errors
        .filter_map(|e| match e {
            Ok(f) => Some(f),
            Err(err) => {
                println!("Error directory {}", err);
                None
            }
        })
        // filter out non-teehistorian files
        .filter_map(|dir_entry| {
            teehistorian_file_uuid(dir_entry.path()).map(|uuid| (dir_entry, uuid))
        })
        .filter(|(_, uuid)| !uuids.contains(uuid));
    for (i, (path, uuid)) in new_teehistorians.enumerate() {
        if i % 1000 == 0 {
            println!("[{}] processing {:?}", i, path);
        }
        match teehistorian_parse_header(path.path()) {
            Ok(mut th) => {
                // get or generate id for path
                let parent = path
                    .path()
                    .parent()
                    .expect("canonical path should have parent")
                    .to_str()
                    .unwrap();
                // Add new teehistorian file to database
                let path_id = if let Some(id) = paths.get(parent) {
                    *id
                } else {
                    conn.execute("INSERT INTO path (name) VALUES (?);", [parent])?;
                    let last_row = conn.last_insert_rowid().try_into().unwrap();
                    paths.insert(parent.to_string(), last_row);
                    last_row
                };
                // fix time stamp to be parseable from sqlite (time zone needs a ':' between '+HH:MM'
                th.start_time.insert(th.start_time.len() - 2, ':');

                let hash = if let Some(sha256) = th.map_sha256 {
                    hex::decode(sha256).unwrap()
                } else {
                    hex::decode(th.map_crc).unwrap()
                };
                conn.execute(
                        "INSERT INTO teehistorian (uuid, path_id, date, map_name, map_sha256) VALUES (?, ?, strftime('%s', ?), ?, ?);",
                    params![
                        uuid.as_bytes(),
                        &path_id,
                        &th.start_time,
                        th.map_name,
                        hash
                    ],
                )?;
            }
            Err(err) => println!("traverse_teehistorian_files: {}", err),
        }
    }
    Ok(())
}

fn get_all_paths(conn: &Connection) -> rusqlite::Result<HashMap<u32, String>> {
    let mut stmt = conn.prepare("SELECT id, name FROM path;")?;
    let paths: Result<_, _> = stmt
        .query_map([], |row| Ok((row.get(0)?, row.get(1)?)))?
        .collect();
    paths
}
fn do_replay(conn: &Connection, opt: Opt) -> rusqlite::Result<()> {
    let paths = get_all_paths(conn)?;
    let mut query =
        "SELECT path_id, uuid, datetime(date, 'unixepoch'), map_name, map_sha256, game_info FROM teehistorian".to_owned();
    let mut conditions = vec![];
    let mut params = vec![];
    if let Some(after) = opt.after {
        conditions.push("date >= strftime('%s', ?)");
        params.push(Value::Text(after));
    }
    if let Some(before) = opt.before {
        conditions.push("date <= strftime('%s', ?)");
        params.push(Value::Text(before));
    }
    if let Some(map_name) = opt.map {
        conditions.push("map_name = ?");
        params.push(Value::Text(map_name));
    }
    query.push_str(" WHERE false");
    if !conditions.is_empty() {
        query.push_str(" OR (");
        query.push_str(&conditions.join(" AND "));
        query.push(')');
    }
    if let Some(uuid) = opt.uuid {
        // could be " OR uuid IN (?,?, ... ?)" if excepting list of uuids
        query.push_str(" OR uuid = ?");
        params.push(Value::Blob(uuid.as_bytes().to_vec()));
    }
    if opt.all {
        query.push_str(" OR true");
    }
    let mut stmt = conn.prepare(&query)?;
    let mut results = stmt.query(rusqlite::params_from_iter(params.iter()))?;
    while let Some(row) = results.next()? {
        let path_id: u32 = row.get(0)?;
        let uuid: [u8; 16] = row.get(1)?;
        let uuid = Uuid::from_bytes(uuid);
        let path = format!("{}/{}.teehistorian", paths.get(&path_id).unwrap(), uuid);
        let date: String = row.get(2)?;
        let map_name: String = row.get(3)?;
        let Ok(map_sha): Result<[u8; 32], _> = row.get(4) else {
            let map_crc: [u8; 4] = row.get(4)?;
            let map_crc = hex::encode(map_crc);
            println!("{path} {date} {map_name} {map_crc}");
            continue;
        };
        let map_sha = hex::encode(map_sha);
        let game_info: Option<String> = row.get(5)?;
        let path = format!("{}/{}.teehistorian", paths.get(&path_id).unwrap(), uuid);
        println!("{path} {date} {map_name} {map_sha} {game_info:?}");

        let f = match File::open(path) {
            Ok(f) => f,
            Err(err) => {
                println!("Error opening teehistorian file: {err}");
                continue;
            }
        };
        let mut th = Th::parse(ThBufReader::new(f)).unwrap();

        let demo = if let Some(demo) = opt.demo.as_ref() {
            let _to = BufWriter::new(File::create(demo).unwrap());
            todo!()
        } else {
            None
        };

        let mut map = if let Some(map) = opt.override_map.as_ref() {
            twmap::TwMap::parse_file(map).unwrap()
        } else {
            todo!()
        };

        let (request_sender, request_receiver) = mpsc::channel();
        let (response_sender, response_receiver) = mpsc::channel();
        let mut world =
            ddnet::DdnetWorld::new(&mut map, request_sender, response_receiver).unwrap();

        let header = ddnet::ThHeader::from_buf(th.header().unwrap());
        world.configure_with_teehistorian_parameters(header);

        let validator = ThValidator::new(&mut world, request_receiver, response_sender);
        let success = validator.validate(th, demo, true);
        println!("Success: {success}");
    }
    Ok(())
}

fn main() {
    // parse arguments
    let opt = Opt::parse();

    // setup directories
    let dirs = BaseDirectories::with_prefix("twgame-replayer").unwrap();
    let db = dirs
        .place_data_file("index_v2.sqlite")
        .expect("failed to create directories");
    println!("Storing index at {db:?}");

    let main_db = setup_db(db).unwrap();

    if let Some(index) = opt.index.clone() {
        println!("Adding teehistorian files from {index}");
        traverse_teehistorian_files(&main_db, &index).unwrap();
    }
    do_replay(&main_db, opt).unwrap();
}

#!/usr/bin/env python3

import os
import sqlite3
import hashlib # sha256
import binascii # crc32
import csv

def get_checksums(bytes):
    sha256 = hashlib.sha256(bytes).hexdigest()
    crc = "{:08x}".format(binascii.crc32(bytes))
    return sha256, crc

def get_checksums_file(filename):
    with open(filename, 'rb') as f:
        bytes = f.read()
        return get_checksums(bytes)

class Map:
    def __init__(self, sha256, crc):
        self.sha256 = sha256
        self.crc = crc

    def __eq__(self, other):
        return self.sha256 == other.sha256

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--teehistorian-db', default='teehistorian.sqlite')
    parser.add_argument('--maps', default='maps')
    parser.add_argument('--output', default='missing.csv')
    parser.add_argument('--map-archive', default='heinrich5991_listing.txt')
    parser.add_argument('--output-archive', default='archive.txt')
    args = parser.parse_args()

    sha256s = {}
    crcs = {}
    for file in os.listdir(args.maps):
        sha256, crc = get_checksums_file(os.path.join(args.maps, file))
        filename_sha256 = file[:-4].rsplit('_', 1)[-1]
        if sha256 != filename_sha256:
            print("Sha256 in filename differs from correct sha256", file, sha256, filename_sha256)
        
        twmap = Map(sha256, crc)
        if sha256 in sha256s:
            print("duplicate map", file, sha256)
        else:
            sha256s[sha256] = twmap

        if crc in crcs:
            if crcs[crc] == twmap:
                print("duplicate crc", file, sha256)
            else:
                print("WARNING: DUPLICATE CRC WITH DIFFERENCT SHA256", file, sha256)
        else:
            crcs[crc] = twmap

    additional_sha256s = {}
    with open(args.map_archive) as f:
        for line in f:
            map_name = line
            map_sha256 = line.rsplit('_', 1)[-1].split('.')[0]
            if not map_sha256 in sha256s:
                if map_sha256 in additional_sha256s:
                    print("duplicate map in archive", map_name, additional_sha256s[map_sha256])
                else:
                    additional_sha256s[map_sha256] = map_name

            print(line)

    print("execute sqlite")
    conn = sqlite3.connect(args.teehistorian_db)
    c = conn.cursor()

    c.execute('''SELECT game_uuid, start_time, map_name, map_sha256, map_size, map_crc, config.value, meta.file_name, meta.file_size
            FROM header
                JOIN config ON header.server_name = config.config_id
                JOIN meta ON header.teehistorian_id = meta.teehistorian_id''')

    only_archive = {}

    with open(args.output, 'w', newline='') as csvfile:
        missing_writer = csv.writer(csvfile, delimiter=',')
        missing_writer.writerow(['game_uuid', 'start_time', 'map_name', 'map_sha256', 'map_size', 'map_crc', 'server_name', 'file_name', 'file_size'])

        for row in c:
            game_uuid, start_time, map_name, map_sha256, map_size, map_crc, server_name, file_name, file_size = row
            if map_sha256 is None:
                if map_crc not in crcs:
                    print("Map-CRC not in repository", row)
                    missing_writer.writerow(row)
            else:
                if map_sha256 not in sha256s:
                    if map_sha256 in additional_sha256s:
                        print("Map not in repository, but in archive", row)
                        only_archive[map_sha256] = additional_sha256s[map_sha256]
                    else:
                        print("Map not in repository", row)
                        missing_writer.writerow(row)

    with open(args.output_archive, "w") as f:
        f.writelines(only_archive.values())


if __name__ == '__main__':
    main()

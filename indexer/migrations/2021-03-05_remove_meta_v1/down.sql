CREATE TABLE meta_v1 (
	meta_id INTEGER PRIMARY KEY NOT NULL,
	teehistorian_id INTEGER,
	parse_time TIMESTAMP NOT NULL,
	file_name TEXT UNIQUE NOT NULL,
	file_size BIGINT NOT NULL, -- same as INTEGER, but allows i64 in diesel
	FOREIGN KEY(teehistorian_id) REFERENCES header(teehistorian_id)
);
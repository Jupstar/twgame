CREATE TABLE known_uuids (
	uuids_id INTEGER PRIMARY KEY NOT NULL,
	uuid_list TEXT UNIQUE NOT NULL
);
CREATE TABLE config (
	config_id INTEGER PRIMARY KEY NOT NULL,
	name TEXT NOT NULL,
	value TEXT NOT NULL,
	tuning INTEGER NOT NULL, -- if 1 represents a "tuning" setting, 0 for "config"
	UNIQUE(name, value, tuning)
);
CREATE TABLE header (
	teehistorian_id INTEGER PRIMARY KEY NOT NULL,
	header_size INTEGER NOT NULL, --size of the header excluding the th-uuid and null-terminator
	version INTEGER NOT NULL,
	game_uuid CHAR(36) UNIQUE NOT NULL,
	version_teeworlds TEXT NOT NULL,
	version_ddnet_major INTEGER NOT NULL,
	version_ddnet_minor INTEGER NOT NULL,
	version_ddnet_patch INTEGER NOT NULL,
	version_commit TEXT, -- builds without a git repository don't store a commit hash
	start_time TIMESTAMP NOT NULL,
	server_name INTEGER NOT NULL,
	server_port INTEGER NOT NULL,
	game_type INTEGER NOT NULL,
	map_name TEXT NOT NULL,
	map_size INTEGER NOT NULL,
	map_sha256 TEXT,
	map_crc TEXT NOT NULL,
	prng_description TEXT,
	uuids INTEGER,
	FOREIGN KEY(server_name) REFERENCES config(config_id), -- always with key "sv_name"
	FOREIGN KEY(game_type) REFERENCES config(config_id), -- always with key "sv_gametype"
	FOREIGN KEY(uuids) REFERENCES known_uuids(uuids_id)
);
CREATE TABLE header_config (
	teehistorian_id INTEGER NOT NULL,
	config_id INTEGER NOT NULL,
	FOREIGN KEY(config_id) REFERENCES config(config_id),
	FOREIGN KEY(teehistorian_id) REFERENCES header(teehistorian_id),
	PRIMARY KEY(config_id, teehistorian_id)
);
CREATE TABLE meta (
	meta_id INTEGER PRIMARY KEY NOT NULL,
	teehistorian_id INTEGER,
	parse_time TIMESTAMP NOT NULL,
	file_name TEXT UNIQUE NOT NULL,
	file_size BIGINT NOT NULL, -- same as INTEGER, but allows i64 in diesel
	FOREIGN KEY(teehistorian_id) REFERENCES header(teehistorian_id)
);
PRAGMA journal_mode=WAL;
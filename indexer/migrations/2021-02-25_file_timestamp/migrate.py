#!/usr/bin/env python3
import sqlite3
import sys
import os
import time

def main():
    if len(sys.argv) != 2:
        print("usage: {} teehistorian.sqlite", sys.argv[0])
        return

    con = sqlite3.connect(sys.argv[1])
    cur = con.cursor()
    cur.execute("SELECT meta_id, teehistorian_id, parse_time, file_name, file_size FROM meta_v1")
    files = cur.fetchall()
    i = 0
    for el in files:
        i += 1
        if i % 10000 == 0:
            print(i)
        modified = os.path.getmtime(el[3])
        modified = time.strftime("%Y-%m-%dT%H:%M:%SZ", time.gmtime(modified))
        cur.execute("INSERT INTO meta (meta_id, teehistorian_id, parse_time, file_name, file_size, file_modified) VALUES (?, ?, ?, ?, ?, ?)", el + (modified,))
    con.commit()
    con.close()

if __name__ == '__main__':
    main()

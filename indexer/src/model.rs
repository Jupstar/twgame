use crate::schema::{config, header, header_config, known_uuids, meta};
use std::collections::HashMap;

use crate::diesel::prelude::*;

#[derive(Insertable)]
#[diesel(table_name = header)]
pub struct NewHeader<'a> {
    pub header_size: i32,
    pub version: i32,
    pub game_uuid: &'a str,
    pub version_teeworlds: &'a str,
    pub version_ddnet_major: i32,
    pub version_ddnet_minor: i32,
    pub version_ddnet_patch: i32,
    pub version_commit: Option<&'a str>,
    pub start_time: &'a str,
    pub server_name: i32,
    pub server_port: i32,
    pub game_type: i32,
    pub map_name: &'a str,
    pub map_size: i32,
    pub map_sha256: Option<&'a String>,
    pub map_crc: &'a str,
    pub prng_description: Option<&'a String>,
    pub uuids: Option<i32>,
}

use diesel::SqliteConnection;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct ThHeader {
    #[serde(deserialize_with = "serde_aux::field_attributes::deserialize_number_from_string")]
    pub version: i32,
    pub game_uuid: String,
    pub server_version: String,
    pub start_time: String,
    pub server_name: String,
    #[serde(deserialize_with = "serde_aux::field_attributes::deserialize_number_from_string")]
    pub server_port: i32,
    pub game_type: String,
    pub map_name: String,
    #[serde(deserialize_with = "serde_aux::field_attributes::deserialize_number_from_string")]
    pub map_size: i32,
    // introduced in 2018-06-05 ca8fcc823cabb769760d39f0b93d0de23552cfc4
    pub map_sha256: Option<String>,
    pub map_crc: String,
    // introduced in 2020-05-25 2692f3c758d884722a76ae113b0de39d9fccb83c
    pub prng_description: Option<String>,
    pub config: HashMap<String, String>,
    pub tuning: HashMap<String, String>,
    pub uuids: Option<Vec<String>>,
}

fn parse_ddnet_version(version: &str) -> Result<(i32, i32, i32), String> {
    let mut parts = version.split_whitespace();
    let mut ddnet_version = parts.next().unwrap().split('.');
    let ddnet_major = ddnet_version
        .next()
        .unwrap()
        .parse::<i32>()
        .map_err(|err| err.to_string())?;
    let ddnet_minor = if let Some(num) = ddnet_version.next() {
        num.parse::<i32>().map_err(|err| err.to_string())?
    } else {
        0
    };
    let ddnet_patch = if let Some(num) = ddnet_version.next() {
        num.parse::<i32>().map_err(|err| err.to_string())?
    } else {
        0
    };
    Ok((ddnet_major, ddnet_minor, ddnet_patch))
}

fn is_at_least_version(v: (i32, i32, i32), other: (i32, i32, i32)) -> bool {
    v.0 > other.0 || (v.0 == other.0 && (v.1 > other.1 || (v.1 == other.1 && v.2 >= other.2)))
}

// parse versions in the form "0.6.4, 15.2.4 (61ef17d0792998e0)" but the first version number and
// the commit hash is optional. This function assumes that the given input has this form.
fn parse_version(version: &str) -> Result<(&str, i32, i32, i32, Option<&str>), String> {
    let mut commit = None;
    let mut tw_version = None;
    let mut ddnet_version = None;
    let mut parse_commit = false;
    for part in version.split(" (") {
        // first parse version then the commit id
        if !parse_commit {
            if part.contains(", ") {
                let mut vs = part.split(", ");
                tw_version = Some(vs.next().unwrap());
                ddnet_version = Some(parse_ddnet_version(vs.next().unwrap())?);
            } else if part == "0.6.4 11.0.2" {
                tw_version = Some("0.6.4");
                ddnet_version = Some((11, 0, 2));
            } else {
                let dv = parse_ddnet_version(part)?;
                ddnet_version = Some(dv);
                // between 11.0.2 and inclusively 11.8.0 broken versions exist. 11.9.0 is the next valid version number
                if is_at_least_version(dv, (11, 0, 2)) && !is_at_least_version(dv, (11, 8, 1)) {
                    tw_version = Some("0.6.4");
                } else if dv == (0, 7, 3) {
                    // early versions from the ddnet7 repository
                    tw_version = Some("0.7.3");
                    // set to -1 to differentiate from versions where 0.9 is set
                    ddnet_version = Some((0, 9, -1)); // all other ddnet7 versions have ddnet version 0.9.0 set
                } else {
                    return Err(format!(
                        "No teeworlds version given at ddnet version {}.{}.{}",
                        dv.0, dv.1, dv.2
                    ));
                }
            }

            // parse the optional available commit hash in the next iteration of the for loop
            parse_commit = true;
        } else {
            // commit is optional
            commit = Some(part.trim_end_matches(')'));
        }
    }
    let ddnet_version = ddnet_version.unwrap();
    Ok((
        tw_version.unwrap(),
        ddnet_version.0,
        ddnet_version.1,
        ddnet_version.2,
        commit,
    ))
}

fn insert_config_id<'a>(
    conn: &mut SqliteConnection,
    name: &'a str,
    value: &'a str,
    tuning: bool,
) -> Result<i32, String> {
    use crate::schema::config::dsl::{
        config as dsl_config, name as config_name, value as config_value,
    };
    let tuning = tuning as i32;
    let results = dsl_config
        .select(config::config_id)
        .filter(config_name.eq(name))
        .filter(config_value.eq(value))
        .load::<i32>(conn)
        .map_err(|err| err.to_string())?;
    if let Some(config_id) = results.first() {
        Ok(*config_id)
    } else {
        diesel::insert_or_ignore_into(config::table)
            .values(NewConfig {
                name,
                value,
                tuning,
            })
            .execute(conn)
            .map_err(|err| err.to_string())?;
        // now the config exists
        let results = dsl_config
            .select(config::config_id)
            .filter(config_name.eq(name))
            .filter(config_value.eq(value))
            .load::<i32>(conn)
            .map_err(|err| err.to_string())?;
        // load shouldn't fail because we just inserted it, therefore unwrap
        let config_id = results.first().unwrap();
        Ok(*config_id)
    }
}

fn insert_config_ids(
    conn: &mut SqliteConnection,
    config: &HashMap<String, String>,
    tuning: bool,
) -> Result<Vec<i32>, String> {
    let mut config_ids: Vec<i32> = vec![];
    for (name, value) in config {
        if name != "sv_motd" {
            let config_id = insert_config_id(conn, name, value, tuning)?;
            config_ids.push(config_id);
        }
    }
    Ok(config_ids)
}

fn get_uuid_list_id(conn: &mut SqliteConnection, uuids: &[String]) -> Result<i32, String> {
    let uuids = serde_json::to_string(uuids).unwrap();
    use crate::schema::known_uuids::dsl::{known_uuids as dsl_known_uuids, uuid_list};
    let results = dsl_known_uuids
        .select(known_uuids::uuids_id)
        .filter(uuid_list.eq(&uuids))
        .load::<i32>(conn)
        .map_err(|err| err.to_string())?;
    if let Some(uuids_id) = results.first() {
        Ok(*uuids_id)
    } else {
        diesel::insert_or_ignore_into(known_uuids::table)
            .values(NewKnownUuids { uuid_list: &uuids })
            .execute(conn)
            .map_err(|err| err.to_string())?;
        // now the config exists
        let results = dsl_known_uuids
            .select(known_uuids::uuids_id)
            .filter(uuid_list.eq(&uuids))
            .load::<i32>(conn)
            .map_err(|err| err.to_string())?;
        // load shouldn't fail because we just inserted it, therefore unwrap
        Ok(*results.first().unwrap())
    }
}

impl<'a> NewHeader<'a> {
    // returns the parsed header and associated config ids to insert
    pub fn from_th_header(
        conn: &mut SqliteConnection,
        header_size: i32,
        th: &'a ThHeader,
    ) -> Result<(Self, Vec<i32>), String> {
        let (tw_version, ddnet_major, ddnet_minor, ddnet_patch, commit) =
            parse_version(&th.server_version)?;
        // get uuids row
        let uuids = if let Some(uuids) = th.uuids.as_ref() {
            Some(get_uuid_list_id(conn, uuids)?)
        } else {
            None
        };
        let server_name = insert_config_id(conn, "sv_name", &th.server_name, false)?;
        let game_type = insert_config_id(conn, "sv_gametype", &th.game_type, false)?;
        // get all config ids to insert
        let mut config_ids: Vec<i32> = insert_config_ids(conn, &th.config, false)?;
        config_ids.extend(insert_config_ids(conn, &th.tuning, true)?);
        config_ids.sort_unstable();
        Ok((
            NewHeader {
                header_size,
                version: th.version,
                game_uuid: &th.game_uuid,
                version_teeworlds: tw_version,
                version_ddnet_major: ddnet_major,
                version_ddnet_minor: ddnet_minor,
                version_ddnet_patch: ddnet_patch,
                version_commit: commit,
                start_time: &th.start_time,
                server_name,
                server_port: th.server_port,
                game_type,
                map_name: &th.map_name,
                map_size: th.map_size,
                map_sha256: th.map_sha256.as_ref(),
                map_crc: &th.map_crc,
                prng_description: th.prng_description.as_ref(),
                uuids,
            },
            config_ids,
        ))
    }
}

#[derive(Insertable)]
#[diesel(table_name = config)]
pub struct NewConfig<'a> {
    pub name: &'a str,
    pub value: &'a str,
    pub tuning: i32,
}

#[derive(Insertable)]
#[diesel(table_name = known_uuids)]
pub struct NewKnownUuids<'a> {
    pub uuid_list: &'a str,
}

#[derive(Insertable)]
#[diesel(table_name = header_config)]
pub struct NewHeaderConfig {
    pub teehistorian_id: i32,
    pub config_id: i32,
}

#[derive(Insertable)]
#[diesel(table_name = meta)]
pub struct NewMeta<'a> {
    pub parse_time: &'a str,
    pub file_name: &'a str,
    pub file_size: i64,
    pub file_modified: &'a str,
    pub teehistorian_id: Option<i32>,
}

#[derive(AsChangeset)]
#[diesel(table_name = meta, treat_none_as_null = true)]
pub struct UpdateMeta<'a> {
    pub teehistorian_id: i32,
    pub parse_time: &'a str,
    pub file_size: i64,
    pub file_modified: &'a str,
}

#[derive(QueryableByName)]
#[diesel(table_name = meta)]
pub struct MetaFileName {
    pub file_name: String,
}

#[derive(QueryableByName)]
#[diesel(table_name = meta)]
pub struct MetaFileNameIds {
    pub file_name: String,
    pub meta_id: i32,
    pub teehistorian_id: Option<i32>,
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn v_eq() {
        assert!(is_at_least_version((15, 0, 4), (15, 0, 4)));
        assert!(is_at_least_version((15, 0, 5), (15, 0, 4)));
        assert!(!is_at_least_version((15, 0, 4), (15, 0, 5)));
        assert!(is_at_least_version((15, 1, 4), (15, 0, 5)));
        assert!(is_at_least_version((15, 1, 5), (15, 0, 4)));
        assert!(!is_at_least_version((15, 0, 5), (15, 1, 4)));
        assert!(!is_at_least_version((15, 0, 4), (15, 1, 5)));

        assert!(is_at_least_version((16, 0, 4), (15, 0, 4)));
        assert!(is_at_least_version((16, 0, 5), (15, 0, 4)));
        assert!(is_at_least_version((16, 0, 4), (15, 0, 5)));
        assert!(is_at_least_version((16, 1, 4), (15, 0, 5)));
        assert!(is_at_least_version((16, 1, 5), (15, 0, 4)));
        assert!(is_at_least_version((16, 0, 5), (15, 1, 4)));
        assert!(is_at_least_version((16, 0, 4), (15, 1, 5)));

        assert!(!is_at_least_version((14, 0, 4), (15, 0, 4)));
        assert!(!is_at_least_version((14, 0, 5), (15, 0, 4)));
        assert!(!is_at_least_version((14, 0, 4), (15, 0, 5)));
        assert!(!is_at_least_version((14, 1, 4), (15, 0, 5)));
        assert!(!is_at_least_version((14, 1, 5), (15, 0, 4)));
        assert!(!is_at_least_version((14, 0, 5), (15, 1, 4)));
        assert!(!is_at_least_version((14, 0, 4), (15, 1, 5)));
    }

    #[test]
    fn parse_version_test() {
        assert_eq!(
            parse_version("0.6.3, 15.2.4 (61ef17d0792998e0)"),
            Ok(("0.6.3", 15, 2, 4, Some("61ef17d0792998e0")))
        );
        assert_eq!(
            parse_version("0.6.3, 15.2.4"),
            Ok(("0.6.3", 15, 2, 4, None))
        );
        assert_eq!(parse_version("0.6.3, 15.2"), Ok(("0.6.3", 15, 2, 0, None)));
        assert_eq!(parse_version("0.6.3, 15"), Ok(("0.6.3", 15, 0, 0, None)));
        assert_eq!(parse_version("11.8"), Ok(("0.6.4", 11, 8, 0, None)));
        assert_eq!(
            parse_version("11.8 (61ef17d0792998e0)"),
            Ok(("0.6.4", 11, 8, 0, Some("61ef17d0792998e0")))
        );
        assert_eq!(
            parse_version("0.6.3, 11.8 (61ef17d0792998e0)"),
            Ok(("0.6.3", 11, 8, 0, Some("61ef17d0792998e0")))
        );
        assert_eq!(parse_version("0.6.3, 11.8"), Ok(("0.6.3", 11, 8, 0, None)));

        assert_eq!(parse_version("11.0.2"), Ok(("0.6.4", 11, 0, 2, None)));
        assert_eq!(parse_version("11.8"), Ok(("0.6.4", 11, 8, 0, None)));
        assert!(parse_version("11.9").is_err());
        assert!(parse_version("11.0.1").is_err());
        assert!(parse_version("12").is_err());
        assert!(parse_version("10").is_err());

        assert_eq!(parse_version("0.6.4 11.0.2"), Ok(("0.6.4", 11, 0, 2, None)));

        assert_eq!(
            parse_version("0.7.3 (274977447bcb6dfd)"),
            Ok(("0.7.3", 0, 9, -1, Some("274977447bcb6dfd")))
        );
        assert_eq!(
            parse_version("0.7.3, 0.9 (274977447bcb6dfd)"),
            Ok(("0.7.3", 0, 9, 0, Some("274977447bcb6dfd")))
        );
        assert_eq!(parse_version("0.7.3, 0.9"), Ok(("0.7.3", 0, 9, 0, None)));
        assert_eq!(parse_version("0.7.3"), Ok(("0.7.3", 0, 9, -1, None)));
    }
}

table! {
    config (config_id) {
        config_id -> Integer,
        name -> Text,
        value -> Text,
        tuning -> Integer,
    }
}

table! {
    header (teehistorian_id) {
        teehistorian_id -> Integer,
        header_size -> Integer,
        version -> Integer,
        game_uuid -> Text,
        version_teeworlds -> Text,
        version_ddnet_major -> Integer,
        version_ddnet_minor -> Integer,
        version_ddnet_patch -> Integer,
        version_commit -> Nullable<Text>,
        start_time -> Timestamp,
        server_name -> Integer,
        server_port -> Integer,
        game_type -> Integer,
        map_name -> Text,
        map_size -> Integer,
        map_sha256 -> Nullable<Text>,
        map_crc -> Text,
        prng_description -> Nullable<Text>,
        uuids -> Nullable<Integer>,
    }
}

table! {
    header_config (header_config_id) {
        header_config_id -> Integer,
        teehistorian_id -> Integer,
        config_id -> Integer,
    }
}

table! {
    known_uuids (uuids_id) {
        uuids_id -> Integer,
        uuid_list -> Text,
    }
}

table! {
    meta (meta_id) {
        meta_id -> Integer,
        teehistorian_id -> Nullable<Integer>,
        parse_time -> Timestamp,
        file_name -> Text,
        file_size -> BigInt,
        file_modified -> Timestamp,
    }
}

joinable!(header -> known_uuids (uuids));
joinable!(header_config -> config (config_id));
joinable!(header_config -> header (teehistorian_id));
joinable!(meta -> header (teehistorian_id));

allow_tables_to_appear_in_same_query!(config, header, header_config, known_uuids, meta,);

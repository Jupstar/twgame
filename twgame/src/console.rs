use std::num::IntErrorKind;
use teehistorian::chunks::ConsoleCommand;

pub enum Command<'a> {
    Team(u32),
    ToggleLock,
    SetLock(bool),
    SaveEmpty,
    Save(&'a [u8]),
    Load(&'a [u8]),
    Kill,
    Team0Mode,
}

impl<'a> Command<'a> {
    pub fn i32(param: &[u8]) -> i32 {
        let _p: i64 = 0;
        // TODO: match function parsing int in ddnet

        // starting with text -> unlock (false)
        // text behind digits -> ignore
        // positive out of range -> lock(true)
        // negative out of range -> unlock (false)
        // u64 % u32::max == 0 -> unlock (false)
        let param: &str = std::str::from_utf8(param).unwrap_or("0");
        match param.parse::<i64>() {
            Ok(i) => i as i32,
            Err(err) => match err.kind() {
                IntErrorKind::PosOverflow => i32::MAX,
                IntErrorKind::NegOverflow => 0,
                _ => 0,
            },
        }
    }

    pub fn from_teehistorian(cmd: ConsoleCommand<'a>) -> Option<Self> {
        match cmd.cmd {
            b"team" => {
                let Some(arg) = cmd.args.first() else {
                    return None;
                };
                let Ok(arg) = std::str::from_utf8(arg) else {
                    return None;
                };
                let Ok(team) = arg.parse::<u32>() else {
                    return None;
                };
                Some(Command::Team(team))
            }
            b"lock" => {
                if let Some(arg) = cmd.args.first() {
                    let i = Command::i32(arg);
                    Some(Command::SetLock(i != 0))
                } else {
                    Some(Command::ToggleLock)
                }
            }
            b"unlock" => Some(Command::SetLock(false)),
            b"load" => cmd.args.first().map(|arg| Command::Load(arg)),
            b"save" => {
                if let Some(arg) = cmd.args.first() {
                    Some(Command::Save(arg))
                } else {
                    Some(Command::SaveEmpty)
                }
            }
            b"kill" => Some(Command::Kill),
            b"team0mode" => Some(Command::Team0Mode),
            _ => None,
        }
    }
}
